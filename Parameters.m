function param = Parameters()
%PARAMETERS Returns parameters for a model of the robot RAMone.
%   
% Generates a structure containing all parameters needed to set up a 
% trajectory optimization problem for finding energy optimal trajectories 
% of a detailed model of the walking robot RAMone.
%
% Input:  - none
% Output: - param: A structure containing the parameters of the robot.

%   Details of the model and of the optimization results can be found in 
%   the paper "The Significance of Robotic Gait Selection: A Case 
%   Study on the Robot RAMone" submitted to Robotics and Automation Letters 
%   2016.

%   The remainder of the code can be found at:
%   https://bitbucket.org/ramlab/ral_2016

%   Created by Nils Smit-Anseeuw (1) on 9-9-16
%   MATLAB 2015b

%   (1) Robotics and Motion Laboratory
%       University of Michigan Ann Arbor
%       nilssmit@umich.edu

%   See also CONTINUOUSDYNAMICSCOSTCONSTRAINTS
%            DISCRETEDYNAMICSCONSTRAINTS
%            ENDCONSTRAINTS

%% Joint mass, inertia and length parameters
% See Figure 1 in the paper for a labeled illustration of the model.

% Main body segment
param.m_1 = 7.9026;       %[kg]         Mass of main body segment 
param.j_1 = 0.08;         %[kgm^2]      Rotational inertia of main body segment 
param.l_H = 0.137675;     %[m]          Distance from main body CoM to hip center

% Thigh segments
param.m_2 = 0.7887;       %[kg]         Mass of thigh segment
param.j_2 = 0.002207;     %[kgm^2]      Rotational inertia of thigh segment
param.l_2 = 0.0193399;    %[m]          Distance from hip center to thigh CoM
param.l_L2 = 0.2;         %[m]          Distance from hip center to knee center

% Shank segments
param.m_3 = 0.510;        %[kg]         Mass of shank segment
param.j_3 = 0.006518;     %[kgm^2]      Rotational inertia of thigh segment
param.l_3 = 0.165235;     %[m]          Distance from knee center to shank CoM
param.l_L3 = 0.2385;      %[m]          Distance from knee center to foot center

% Feet
param.r_foot = 0.0282;    %[m]          Radius of foot 

%% Series elastic elements parameters
% See Equations 1 and 2 in the paper for the governing equations of the
% series elastic elements.

% Hip springs
param.k_alpha = 70;       %[Nm/rad]     Spring constant of hip springs
param.b_alpha = 7;        %[Nm/(rad/s)] Damping constant of hip springs

% Knee springs
param.k_beta1 = 80;       %[Nm/rad]     Spring constant of knee spring endstops
param.k_beta2 = 36;       %[Nm/rad]     Spring constant of knee springs
param.b_beta1 = 10;       %[Nm/(rad/s)] Damping constant of knee spring endstops
param.b_beta2 = 0.3;      %[Nm/(rad/s)] Damping constant of knee spring endstops
param.f_beta = 0.55;      %[Nm]         Dry friction constant of knee springs
param.beta_sm = -0.023;    %[rad]        Angle at which knee collides with endstop

%% Actuator parameters
% See Equation 3 in the paper for the motor dynamics. See Equation 5 for
% the electrical cost of driving the motors.

param.j_rot = 1.21e-4;    %[kgm^2]      Inertia of the rotors
param.S_mot = 8.4e-3;     %[Nm/(rad/s)] Speed torque gradient of the motors
param.n_alpha = 50;       %[]           Net gear ratio of the hip motors
param.n_beta = 53.125;      %[]           Net gear ratio of the knee motors

%% Joint and actuator constraint parameters

% Height constraint
param.y_min = 0.1;        %[m]          Minimum main body height
param.y_max = 1;          %[m]          Maximum main body height

% Pitch constraint
param.phi_min = -pi/2;    %[rad]        Minimum body pitch
param.phi_max = pi/2;     %[rad]        Maximum body pitch

% Hip constraint
param.alpha_min = -pi/2;  %[rad]        Minimum hip angle
param.alpha_max = pi/2;   %[rad]        Maximum hip angle

% Knee constraint
param.beta_min = -3*pi/4; %[rad]        Minimum knee angle
param.beta_max = 0;       %[rad]        Maximum knee angle

% Motor constraints
param.T_rot_max = 0.568;  %[Nm]         Maximum rotor torque
param.du_rot_max = 838;   %[rad/s]      Maximum rotor velocity torque

%% Smoothing parameter (Used to smooth nondifferentiable functions in the dynamics and cost)
param.sigma   = 0.001;    %[]           Logistic constant for smoothing functions

end
