function val = MinSmooth(x,sigma)
%MINSMOOTH returns the smoothed approximation of the function min(0,x) given
%by -sigma*log(1+exp(-x/sigma))
%   [val] = MinSmooth(x,sigma) returns the value of the smoothed function
%   min(0,x) for a vector x and a smoothing constant sigma > 0. The 
%   smaller the sigma, the tighter the approximation.
	
    % Get x/sigma
    y = x/sigma;
    
    % Manually set the output to the exact function for values of x much
    % larger than sigma. We do this to avoid issues with calculating
    % exp(large number)
    lin_ind = y<-10;
    val(lin_ind) = x(lin_ind);
    
    % Calculate the smoothed function value for|x| < 10*sigma
    smooth_ind = y>=-10;
    val(smooth_ind) = -sigma*log(1+exp(-y(smooth_ind)));
end