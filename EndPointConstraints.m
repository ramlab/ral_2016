function eq_constr = EndPointConstraints(q_0, dq_0, u_0, du_0, q_tf, dq_tf, u_tf, du_tf, tf, v_des)
%EndPointConstraints Computes the endpoint constraints for a trajectory 
%optimization with a model of the robot RAMone
%
% Input:  - q_*: The configuration of the robot (7x1 vector)
%         - dq_*: the configuration velocity of the robot (7x1 vector)
%         - u_*: The motor positions of the robot (4x1 vector)
%         - du_*: The motor velocities of the robot (4x1 vector)
%         - *_0: Robot states at time = 0
%         - *_tf: Robot states at time = tf
% 
% Output: - eq_constr: The equality constraint vector (Nx1 vector)

%   Details of the model and of the optimization results can be found in 
%   the paper "The Significance of Robotic Gait Selection: A Case 
%   Study on the Robot RAMone" submitted to Robotics and Automation Letters 
%   2016.

%   The remainder of the code can be found at:
%   https://bitbucket.org/ramlab/ral_2016

%   Created by Nils Smit-Anseeuw (1) on 9-9-16
%   MATLAB 2015b

%   (1) Robotics and Motion Laboratory
%       University of Michigan Ann Arbor
%       nilssmit@umich.edu

%   See also PARAMETERS
%            DISCRETEDYNAMICSCONSTRAINTS
%            ENDCONSTRAINTS
%% Symmetric periodicity constraints
% Since we are only concerned with left/right symmetric gaits, we enforce a
% mirrored periodicity condition on the trajectories. That is, if you
% exchanged left and right legs at the end state, you match the initial state.
%
% This corresponds to Eq. 4k in the paper

q_mirr_ind = [1:3, 6 7 4 5]; % The configuration indices for the leg-mirrored state
u_mirr_ind = [3 4 1 2]; % The motor indices for the leg-mirrored state

per_con = [ q_0(2:end) -  q_tf(q_mirr_ind(2:end)) % Note that we don't enforce periodicity on x
           dq_0        - dq_tf(q_mirr_ind) 
            u_0        -  u_tf(u_mirr_ind)
           du_0        - du_tf(u_mirr_ind) ];
           
%% Velocity constraint
% To enforce the velocity constraint v = v_des, we fix the x position of
% the robot at time 0 to be 0, and the x position of the robot at time tf
% to be v_des*tf.
%
% This corresponds to Eq. 4l in the paper

vel_con = [ q_0(1)  - 0
            q_tf(1) - v_des*tf];
        
%% Load the constraints into a vector
eq_constr = [per_con;
             vel_con];

end