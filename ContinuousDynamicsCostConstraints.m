function [ ddq, ddu, dcost, neq_constr ] = ContinuousDynamicsCostConstraints( q, dq, u, du, T_mot, contact, p )
%CONTINUOUSDYNAMICSCOSTCONSTRAINTS Computes the continuous dynamics, cost 
%and constraints for a model of the robot RAMone
%   
% Input:  - q: The configuration of the robot (7x1 vector)
%         - dq: the configuration velocity of the robot (7x1 vector)
%         - u: The motor positions of the robot (4x1 vector)
%         - du: The motor velocities of the robot (4x1 vector)
%         - T_mot: The torque on the motors (4x1 vector)
%         - contact: The contact configuration. If contact.L(R) = 1, then
%                    the left(right) foot is in contact with the ground.
%                    Otherwise the foot is in the air. (struct)
%         - p: A structure containing the parameters of the robot. This is
%              gneerated with the function Parameters.m. (struct) 
%
% Output: - ddq: The acceleration of the robot configuration (7x1 vector)
%         - ddu: The acceleration of the robot motors (4x1 vector)
%         - dcost: The rate of change of the cost function (scalar)
%         - neq_constr: The inequality constraint vector (Nx1 vector)

%   Details of the model and of the optimization results can be found in 
%   the paper "The Significance of Robotic Gait Selection: A Case 
%   Study on the Robot RAMone" submitted to Robotics and Automation Letters 
%   2016.

%   The remainder of the code can be found at:
%   https://bitbucket.org/ramlab/ral_2016

%   Created by Nils Smit-Anseeuw (1) on 9-9-16
%   MATLAB 2015b

%   (1) Robotics and Motion Laboratory
%       University of Michigan Ann Arbor
%       nilssmit@umich.edu

%   See also PARAMETERS
%            DISCRETEDYNAMICSCONSTRAINTS
%            ENDCONSTRAINTS

%% Call the autogenerated functions to get the mass matrix, coriolis/gravitational forces and contact Jacobian
% The auto generated functions are generated with the function
% SymbolicComputationofEOM.m and are stored in the folder
% AutoGeneratedFunctions. 

addpath(fullfile('.','Utilities','AutogeneratedFcts'))

p_vec = [p.m_1 p.m_2 p.m_3 p.l_H p.l_L2 p.l_L3 p.l_2 p.l_3 p.r_foot p.j_1 p.j_2 p.j_3];

M = MassMatrix(q', p_vec);

f = CoriGravForces(q', dq', p_vec);

J = [];
dJ = [];
if contact.R
    JR = JacobianR(q', p_vec); % Right foot contact Jacobian
    dJR = dJacobianR(q', dq', p_vec); % Time derivative of right foot contact Jacobian
    J = [J;JR];
    dJ = [dJ;dJR];
end
if contact.L
    JL = JacobianL(q', p_vec); % Left foot contact Jacobian
    dJL = dJacobianL(q', dq', p_vec); % Time derivative of left foot contact Jacobian
    J = [J;JL];
    dJ = [dJ;dJL];
end

%% Calculate the torques in the series elastic actuators

addpath(fullfile('.','Utilities'))

tau = ComputeSEATorques(q, dq, u, du, p);

%% Compute the contact forces and ddq
% We compute the contact forces for all (if any) active contacts by
% implicitly solving the system of equations:
%   M*ddq - J'*lambda = f + tau    (Dynamics)
%   d(J*dq) = dJ*dq + J*ddq = 0    (Zero acceleration of the contact point)
% We solve the system as follows
%   M*ddq - J'*lambda = f + tau
%   -> J*inv(M)*( M*ddq - J'*lambda ) = J*inv(M)*(f + tau)
%   -> J*ddq - J*inv(M)*J'*lambda = J*inv(M)*(f + tau)
%   -> -dJ*dq - J*inv(M)*J'*lambda = J*inv(M)*(f + tau)
%   -> lambda = -inv(J*inv(M)*J')*( J*inv(M)*(f + tau) + dJ*dq )
% Once we have lambda, we can solve for ddq:
%   M*ddq - J'*lambda = f + tau
%   -> ddq = inv(M)*(f + tau + J'*lambda)

if ~isempty(J)
    lambda = -(J * (M \ (J'))) \ ( J * (M \ (f + [0;0;0;tau])) + dJ*dq);
    ddq = M \ (f + [0;0;0;tau] + J'*lambda);
else
    %no active constraints
    lambda = [];
    ddq = M \ (f + [0;0;0;tau]);
end

%% Compute ddu
% ddu is calculated based on the reflected inertia of the motors, joint
% torques and motor torques (see Eq. 3 in the paper)

% Gear ratio matrix
N = diag([p.n_alpha;p.n_beta;p.n_alpha;p.n_beta]);

ddu = (N^2*p.j_rot)\(T_mot - tau);

%% Compute the cost derivative
% The time derivative of the cost function is given by the positive 
% electrical power needed to drive the motors (see Eq. 5 in the paper).

% The electrical power is given by the sum of the copper losses and
% mechanical work performed by the motors:
pElecTotal = T_mot'*((p.S_mot*N^2)\T_mot) + T_mot'*du;

% To get the positive electrical power, we want max(0,pElecTotal). However,
% the function max(0,x) is not differentiable, which can cause problems in
% optimization. Instead we smooth this function using a logistic 
% approximation of the function min(0,x) (see utilities\MINSMOOTH.m)
dcost = - MinSmooth( -pElecTotal, p.sigma);

%% Compute the constraints
% Throughout the continuous phases of the trajectory, the following
% inequality constraint must be satisfied: [neq_constr >= 0] 
% Where neq_constraint is given by:

% Joint limits
%
% This corresponds to Eq. 4c in the paper.

q_lim = [  q(2) - p.y_min; % y limits
          -q(2) + p.y_max;                          
           q(3) - p.phi_min; % phi limits   
          -q(3) + p.phi_max;
           q([4, 6]) - p.alpha_min; % left and right hip joint limits           
          -q([4, 6]) + p.alpha_max;
           q([5, 7]) - p.beta_min; % left and right knee joint limits
          -q([5, 7]) + p.beta_max];
          
% Motor velocity and torque limits (rotor velocity and torque limits scaled
% by the hip and knee gearboxes)
%
% This corresponds to Eq. 4d in the paper.

act_lim = [  du([1, 3]) + p.du_rot_max/p.n_alpha; % Hip motor speed limits
            -du([1, 3]) + p.du_rot_max/p.n_alpha;
             du([2, 4]) + p.du_rot_max/p.n_beta; % Knee motor speed limits
            -du([2, 4]) + p.du_rot_max/p.n_beta;
             T_mot([1, 3]) + p.T_rot_max*p.n_alpha; % Hip torque limits
            -T_mot([1, 3]) + p.T_rot_max*p.n_alpha;
             T_mot([2, 4]) + p.T_rot_max*p.n_beta; % Knee torque limits
            -T_mot([2, 4]) + p.T_rot_max*p.n_beta];

% Foot ground clearance constraint (swing feet can't cross the ground). 
% These are computed with the auto-generated functions ContactPointR and
% ContactPointL
%
% This corresponds to Eq. 4e in the paper.

foot_clear = [];
if ~contact.R
    footR_pos = ContactPointR(q', p_vec);
    foot_clear = [foot_clear;
                  footR_pos(2)];
end
if ~contact.L
    footL_pos = ContactPointL(q', p_vec);
    foot_clear = [foot_clear;
                  footL_pos(2)];
end

% Positive ground contact force constraints (feet can't 'stick' to the ground)
%
% This corresponds to Eq. 4f in the paper.

pos_GCF = lambda(2:2:end);  

%% Load the inequality constraints into a single vector
neq_constr = [ q_lim;
               act_lim;
               foot_clear;
               pos_GCF];
end